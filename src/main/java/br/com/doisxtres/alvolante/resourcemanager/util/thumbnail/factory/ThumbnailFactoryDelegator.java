package br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ThumbnailFactoryDelegator implements ThumbnailFactory {
	private List<ThumbnailFactory> factories = new ArrayList<>();
	
	public ThumbnailFactoryDelegator(ThumbnailFactory... factories) {
		for (ThumbnailFactory factory : factories) {
			this.factories.add(factory);
		}
	}
	
	public void registerFactories(ThumbnailFactory... factories) {
		for (ThumbnailFactory factory : factories) {
			this.factories.add(factory);
		}
	}
	
	public void removeFactories(ThumbnailFactory... factories) {
		for (ThumbnailFactory factory : factories) {
			this.factories.remove(factory);
		}
	}
	
	@Override
	public boolean accept(File source) {
		for (ThumbnailFactory factory : factories) {
			if (factory.accept(source)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public File create(File source) throws IOException {
		for (ThumbnailFactory factory : factories) {
			if (factory.accept(source)) {
				return factory.create(source);
			}
		}
		return null;
	}
}
