package br.com.doisxtres.alvolante.resourcemanager.core.network;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.http.entity.mime.content.FileBody;

import br.com.doisxtres.alvolante.resourcemanager.core.upload.CountingOutputStream;
import br.com.doisxtres.alvolante.resourcemanager.model.Resource;

public class ResourceFileBody extends FileBody {
	private final Resource resource;
	private final long initialCount;
	
	public ResourceFileBody(Resource resource) {
		this(resource, resource.getFile(), 0);
	}
	
	public ResourceFileBody(Resource resource, File file, long initialCount) {
		super(file);
		this.resource = resource;
		this.initialCount = initialCount;
	}
	
	@Override
	public void writeTo(OutputStream out) throws IOException {
		CountingOutputStream countingOut = new CountingOutputStream(out, initialCount);
		resource.getUploadedSizeProperty().bind(countingOut.countProperty());
		super.writeTo(countingOut);
	}
}
