package br.com.doisxtres.alvolante.resourcemanager;

import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Config {
	private static final Logger LOGGER = Logger.getLogger(Config.class.getName());
	private static final Config INSTANCE = new Config();

	private final Properties properties = new Properties();

	private Config() {
		try (InputStream is = Config.class.getResourceAsStream("/config.properties")) {
			properties.load(is);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Failed to load config from file", e);
		}
	}

	private static String getProperty(String key, String defaultValue) {
		return INSTANCE.properties.getProperty(key, defaultValue);
	}

	private static int getPropertyInt(String key, int defaultValue) {
		String property = INSTANCE.properties.getProperty(key);
		if (property != null) {
			try {
				return Integer.parseInt(property);
			} catch (NumberFormatException e) { }
		}
		return defaultValue;
	}

	public static String getRemoteHost() {
		return getProperty("connection.host", "localhost");
	}

	public static int getRemotePort() {
		return getPropertyInt("connection.port", -1);
	}

	public static String getRemoteSchema() {
		return getProperty("connection.schema", "http");
	}

	public static final long getUploadRetryInterval() {
		return 30000; // 30 s
	}

	public static boolean isMultipartUploadEnabled() {
		return true;
	}

	public static long getMultipartUploadMinFileSize() {
		return 20 * 1024 * 1024; // 20mb
	}

	public static long getMultipartUploadPartSize() {
		return 10 * 1024 * 1024; // 10mb
	}
}
