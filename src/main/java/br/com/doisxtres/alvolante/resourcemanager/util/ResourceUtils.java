package br.com.doisxtres.alvolante.resourcemanager.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import br.com.doisxtres.alvolante.resourcemanager.model.Resource;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceContainer;

public final class ResourceUtils {
	private ResourceUtils() { }
	
	public static ResourceContainer loadPages(File file) throws IOException {
		return loadPages(file, true, true);
	}
	
	public static ResourceContainer loadPages(File file, boolean strictDirectoryCheck, boolean ignoreEmptyResources) throws IOException {
		if (file == null || !file.exists()) {
			throw new FileNotFoundException();
		}
		if (file.isHidden()) {
			return null;
		}
		if (!file.isDirectory()) {
			if (!strictDirectoryCheck) {
				return null;
			}
			throw new IOException("Incorrect directories hierarchy. A file found where supposed to be only directories: " + file.getAbsolutePath());
		}
		
		ResourceContainer pages = new ResourceContainer("Páginas");

		for (String pageName : file.list()) {
			File pageFile = new File(file, pageName);
			ResourceContainer page = loadPage(pageFile, strictDirectoryCheck, ignoreEmptyResources);
			if (page != null) {
				pages.addResource(page);
			}
		}
		
		if (pages.getResources().isEmpty() && ignoreEmptyResources) {
			return null;
		}

		return pages;
	}

	public static ResourceContainer loadPage(File file, boolean strictDirectoryCheck, boolean ignoreEmptyResources) throws IOException {
		if (file == null || !file.exists()) {
			throw new FileNotFoundException();
		}
		if (file.isHidden()) {
			return null;
		}
		if (!file.isDirectory()) {
			if (!strictDirectoryCheck) {
				return null;
			}
			throw new IOException("Failed to load page: Incorrect directories hierarchy. A file found where supposed to be only directories: " + file.getAbsolutePath());
		}
		
		ResourceContainer page = new ResourceContainer(file);

		for (String subpageName : file.list()) {
			File subpageFile = new File(file, subpageName);
			ResourceContainer subpage = loadSubpage(subpageFile, strictDirectoryCheck, ignoreEmptyResources);
			if (subpage != null) {
				page.addResource(subpage);
			}
		}
		
		if (page.getResources().isEmpty() && ignoreEmptyResources) {
			return null;
		}

		return page;
	}

	public static ResourceContainer loadSubpage(File file, boolean strictDirectoryCheck, boolean ignoreEmptyResources) throws IOException {
		if (file == null || !file.exists()) {
			throw new FileNotFoundException();
		}
		if (file.isHidden()) {
			return null;
		}
		if (!file.isDirectory()) {
			if (!strictDirectoryCheck) {
				return null;
			}
			throw new IOException("Failed to load subpage: Incorrect directories hierarchy. A file found where supposed to be only directories: " + file.getAbsolutePath());
		}
		
		ResourceContainer subpage = new ResourceContainer(file);

		for (String sectionName : file.list()) {
			File sectionFile = new File(file, sectionName);
			ResourceContainer section = loadSection(sectionFile, strictDirectoryCheck, ignoreEmptyResources);
			if (section != null) {
				subpage.addResource(section);
			}
		}
		
		if (subpage.getResources().isEmpty() && ignoreEmptyResources) {
			return null;
		}

		return subpage;
	}

	public static ResourceContainer loadSection(File file, boolean strictDirectoryCheck, boolean ignoreEmptyResources) throws IOException {
		if (file == null || !file.exists()) {
			throw new FileNotFoundException();
		}
		if (file.isHidden()) {
			return null;
		}
		if (!file.isDirectory()) {
			if (!strictDirectoryCheck) {
				return null;
			}
			throw new IOException("Failed to load section: Incorrect directories hierarchy. A file found where supposed to be only directories: " + file.getAbsolutePath());
		}
		
		ResourceContainer section = new ResourceContainer(file);

		for (String resourceName : file.list()) {
			File resourceFile = new File(file, resourceName);
			Resource resource = loadResource(resourceFile, strictDirectoryCheck, ignoreEmptyResources);
			if (resource != null) {
				section.addResource(resource);
			}
		}
		
		if (section.getResources().isEmpty() && ignoreEmptyResources) {
			return null;
		}

		return section;
	}

	public static Resource loadResource(File file, boolean strictDirectoryCheck, boolean ignoreEmptyResources) throws IOException {
		if (file == null || !file.exists()) {
			throw new FileNotFoundException();
		}
		if (file.isHidden()) {
			return null;
		}
		if (file.isDirectory()) {
			if (!strictDirectoryCheck) {
				return null;
			}
			throw new IOException("Failed to load resource: Incorrect directories hierarchy. A directory found where supposed to be only files: " + file.getAbsolutePath());
		}
		
		Resource resource = new Resource(file);
		return resource;
	}
	
	public static void merge(ResourceContainer target, ResourceContainer resources) {
		if (target == null || resources == null) {
			return;
		}
		for (Resource resource : resources.getResources()) {
			if (target.addResource(resource)) {
				continue;
			}
			
			Resource targetResource = target.findResource(resource);
			if (targetResource instanceof ResourceContainer) {
				merge((ResourceContainer) targetResource, (ResourceContainer) resource);
			}
		}
	}
}
