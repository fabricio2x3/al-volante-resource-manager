package br.com.doisxtres.alvolante.resourcemanager.core;

public interface AsyncCallback<T> {
	void completed(T response);
	void failed(Exception cause);
}
