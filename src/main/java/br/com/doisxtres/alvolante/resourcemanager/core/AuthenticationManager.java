package br.com.doisxtres.alvolante.resourcemanager.core;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import br.com.doisxtres.alvolante.resourcemanager.core.network.NetworkService;
import br.com.doisxtres.alvolante.resourcemanager.model.User;

public class AuthenticationManager implements ObservableValue<User> {
	private static final Logger LOGGER = Logger.getLogger(AuthenticationManager.class.getName());
	
	private final ObjectProperty<User> loggedUser = new SimpleObjectProperty<>();

	public User getLoggedUser() {
		return loggedUser.get();
	}

	public void setLoggedUser(User loggedUser) {
		this.loggedUser.set(loggedUser);
	}
	
	public void login(String username, String password, AsyncCallback<User> callback) {
		NetworkService.getInstance().login(username, password);
		ThreadPoolManager.getInstance().executeAsync(() -> NetworkService.getInstance().getUser(), new AsyncCallback<User>() {
			@Override
			public void completed(User response) {
				LOGGER.log(Level.INFO, AuthenticationManager.class.getSimpleName() + ": Login success");
				callback.completed(response);
				Platform.runLater(() -> setLoggedUser(response));
			}

			@Override
			public void failed(Exception cause) {
				LOGGER.log(Level.WARNING, AuthenticationManager.class.getSimpleName() + ": Login failed", cause);
				callback.failed(cause);
			}
		});
	}
	
	public void logout() {
		try {
			NetworkService.getInstance().logout();
			Platform.runLater(() -> setLoggedUser(null));
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, AuthenticationManager.class.getSimpleName() + ": Logout failed", e);
		}
	}
	
	@Override
	public void addListener(InvalidationListener listener) {
		loggedUser.addListener(listener);
	}

	@Override
	public void removeListener(InvalidationListener listener) {
		loggedUser.removeListener(listener);
	}

	@Override
	public void addListener(ChangeListener<? super User> listener) {
		loggedUser.addListener(listener);
	}

	@Override
	public void removeListener(ChangeListener<? super User> listener) {
		loggedUser.removeListener(listener);
	}

	@Override
	public User getValue() {
		return getLoggedUser();
	}
	
	public static AuthenticationManager getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder {
		protected static final AuthenticationManager INSTANCE = new AuthenticationManager();
	}
}
