package br.com.doisxtres.alvolante.resourcemanager.view.scene.control.cell;

import java.util.Date;

import javafx.beans.binding.StringBinding;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.util.Callback;
import br.com.doisxtres.alvolante.resourcemanager.util.Utils;

public class DateTreeTableCell<S> extends TreeTableCell<S, Date> {
	public static <S> Callback<TreeTableColumn<S, Date>, TreeTableCell<S, Date>> forTreeTableColumn() {
		return param -> new DateTreeTableCell<S>();
	}
	
	private final Label label = new Label();
	
	public DateTreeTableCell() {
		
	}
	
	@Override
	protected void updateItem(Date item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setGraphic(null);
		} else {
			label.textProperty().unbind();

			final TreeTableColumn<S, Date> column = getTableColumn();
			final ObservableValue<Date> observable = column == null ? null : column.getCellObservableValue(getIndex());

			if (observable != null) {
				label.textProperty().bind(new LabelBinding(observable));
			} else if (item != null) {
				label.setText(Utils.formatDateTime(item));
			}

			setGraphic(label);
		}
	}
	
	protected static class LabelBinding extends StringBinding {
		private final ObservableValue<Date> observable;
		
		public LabelBinding(ObservableValue<Date> observable) {
			this.observable = observable;
			bind(this.observable);
		}
		
		@Override
		protected String computeValue() {
			Date value = observable.getValue();
			return value == null ? null : Utils.formatDateTime(value);
		}
	}
}
