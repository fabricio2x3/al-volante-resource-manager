package br.com.doisxtres.alvolante.resourcemanager.core.upload;

import java.io.Serializable;

public class MultipartUploadIdentifier implements Serializable {
	private static final long serialVersionUID = -5156045781364283217L;

	private String tmpPath;
	private String uploadId;

	public String getTmpPath() {
		return tmpPath;
	}

	public void setTmpPath(String tmpPath) {
		this.tmpPath = tmpPath;
	}

	public String getUploadId() {
		return uploadId;
	}

	public void setUploadId(String uploadId) {
		this.uploadId = uploadId;
	}
}
