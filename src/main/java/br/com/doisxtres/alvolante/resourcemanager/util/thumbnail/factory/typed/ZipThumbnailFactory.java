package br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.typed;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.imageio.ImageIO;

import br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.RegexThumbnailFactory;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

public class ZipThumbnailFactory extends RegexThumbnailFactory {
	public ZipThumbnailFactory() {
		super("(.*(\\.(?i)(zip|rar|tar.gz))$)");
	}

	@Override
	protected File createImpl(File source) throws IOException {
		try (ZipInputStream zin = new ZipInputStream(new FileInputStream(source))) {
			ZipEntry entry = null;
			while ((entry = zin.getNextEntry()) != null) {
				if (!entry.isDirectory() && accept(entry.getName())) {
					File pdf = File.createTempFile(entry.getName(), "");
					extractFile(zin, pdf.getAbsolutePath());
					return getThumbnailForPdf(pdf);
				}
			}
		}
		return null;
	}

	private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
		try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath))) {
			byte[] bytesIn = new byte[4096];
			int read = 0;
			while ((read = zipIn.read(bytesIn)) != -1) {
				bos.write(bytesIn, 0, read);
			}
		}
	}
	
	private static File getThumbnailForPdf(File source) throws IOException {
		RandomAccessFile raf = new RandomAccessFile(source, "r");
		FileChannel channel = raf.getChannel();
		ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
		PDFFile pdf = new PDFFile(buf);
		PDFPage page = pdf.getPage(0);
		File output = File.createTempFile("thumbnail", ".jpg");

		// create the image
		Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(), (int) page.getBBox().getHeight());
		BufferedImage bufferedImage = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);

		Image image = page.getImage(rect.width, rect.height, rect, null, true, true);
		Graphics2D bufImageGraphics = bufferedImage.createGraphics();
		bufImageGraphics.drawImage(image, 0, 0, null);

		ImageIO.write(bufferedImage, "jpeg", output);
		raf.close();

		return output;
	}
}
