package br.com.doisxtres.alvolante.resourcemanager.core;

import java.io.IOException;

public class LocalizedIOException extends IOException {
	private static final long serialVersionUID = 4144076514828478655L;
	
	private final LocalizedMessage localizedMessage;
	
	public LocalizedIOException(LocalizedMessage localizedMessage) {
		this.localizedMessage = localizedMessage;
	}

	public LocalizedIOException(String message, Throwable cause, LocalizedMessage localizedMessage) {
		super(message, cause);
		if (localizedMessage == null) {
			throw new NullPointerException("Localized message must not be null!");
		}
		this.localizedMessage = localizedMessage;
	}

	public LocalizedIOException(String message, LocalizedMessage localizedMessage) {
		super(message);
		if (localizedMessage == null) {
			throw new NullPointerException("Localized message must not be null!");
		}
		this.localizedMessage = localizedMessage;
	}

	public LocalizedIOException(Throwable cause, LocalizedMessage localizedMessage) {
		super(cause);
		if (localizedMessage == null) {
			throw new NullPointerException("Localized message must not be null!");
		}
		this.localizedMessage = localizedMessage;
	}
	
	@Override
	public String getLocalizedMessage() {
		return localizedMessage.toString();
	}
}
