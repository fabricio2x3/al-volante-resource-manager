package br.com.doisxtres.alvolante.resourcemanager.core.network;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.impl.client.AbstractResponseHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonResponseHandler<T> extends AbstractResponseHandler<T> {
	private final Class<T> clazz;
	private final ObjectMapper objectMapper = new ObjectMapper();

	public JsonResponseHandler(Class<T> clazz) {
		this.clazz = clazz;
	}

	@Override
	public T handleEntity(HttpEntity entity) throws IOException {
		return objectMapper.readValue(entity.getContent(), clazz);
	}

	protected Class<T> getClazz() {
		return clazz;
	}

	protected ObjectMapper getObjectMapper() {
		return objectMapper;
	}
}
