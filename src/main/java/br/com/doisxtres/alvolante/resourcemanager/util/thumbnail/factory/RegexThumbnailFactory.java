package br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

public abstract class RegexThumbnailFactory implements ThumbnailFactory {
	private final Pattern pattern;
	
	public RegexThumbnailFactory(String regex) {
		pattern = Pattern.compile(regex);
	}
	
	@Override
	public final boolean accept(File source) {
		return accept(source.getName());
	}
	
	public final boolean accept(String sourceFileName) {
		return pattern.matcher(sourceFileName).matches();
	}

	@Override
	public File create(File source) throws IOException {
		if (!accept(source)) {
			return null;
		}
		return createImpl(source);
	}

	protected abstract File createImpl(File source) throws IOException;
}
