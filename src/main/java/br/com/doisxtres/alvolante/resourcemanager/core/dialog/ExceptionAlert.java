package br.com.doisxtres.alvolante.resourcemanager.core.dialog;

import java.io.PrintWriter;
import java.io.StringWriter;

import br.com.doisxtres.alvolante.resourcemanager.ApplicationMain;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class ExceptionAlert extends Alert {
	
	public ExceptionAlert(Exception exception) {
		super(AlertType.ERROR);
		
		setTitle(ApplicationMain.getMessage("error"));
		setHeaderText(null);
		setContentText(exception.getLocalizedMessage());
		
		// Create expandable Exception.
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		exception.printStackTrace(pw);
		final String exceptionText = sw.toString();
		
		final Label label = new Label(ApplicationMain.getMessage("exception.stacktrace"));
		final TextArea textArea = new TextArea(exceptionText);
		textArea.setEditable(false);
		textArea.setWrapText(true);
		textArea.setMaxWidth(Double.MAX_VALUE);
		textArea.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);
		
		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);
		
		getDialogPane().setExpandableContent(expContent);
	}

}
