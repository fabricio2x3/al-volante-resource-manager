package br.com.doisxtres.alvolante.resourcemanager.model;

public enum ResourceStatusType {
	INITIAL,
	CREATED,
	QUEUED,
	UPLOADING,
	UPLOAD_SUSPENDED,
	UPLOADED,
	GENERATING_THUMBNAIL,
	UPLOADING_THUMBNAIL,
	FINISHING,
	FINISHED,
	ERROR
}
