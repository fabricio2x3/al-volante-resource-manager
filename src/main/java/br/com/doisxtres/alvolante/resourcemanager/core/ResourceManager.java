package br.com.doisxtres.alvolante.resourcemanager.core;

import java.io.IOException;

import br.com.doisxtres.alvolante.resourcemanager.core.network.NetworkService;
import br.com.doisxtres.alvolante.resourcemanager.model.RemoteResource;
import br.com.doisxtres.alvolante.resourcemanager.model.Resource;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceContainer;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceStatusType;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceType;

public final class ResourceManager {
	protected ResourceManager() { }

	public void syncAll(Resource resource) throws IOException {
		syncContainers(resource);
		syncResources(resource);
	}

	public void sync(Resource resource) throws IOException {
		try {
			RemoteResource remote = null;
			switch (resource.getType()) {
				case ROOT:
					return;
				case PAGE:
					remote = NetworkService.getInstance().findPage(resource.getName());
					break;
				case SUBPAGE:
					remote = NetworkService.getInstance().findSubpage(resource.getParent().getId(), resource.getName());
					break;
				case SECTION:
					remote = NetworkService.getInstance().findSection(resource.getParent().getId(), resource.getName());
					break;
				case RESOURCE:
					// Prevent re-creating resources
					if (resource.getId() != null) {
						return;
					}
					remote = NetworkService.getInstance().createResource(resource.getParent().getId(), resource.getName());
					break;
			}

			if (remote == null) {
				throw new NullPointerException();
			}

			update(resource, remote);
		} catch (IOException e) {
			resource.getStatus().setStatus(ResourceStatusType.ERROR, e);
			throw new IOException("Something went wrong while trying to sync resource: " + resource, e);
		}
	}

	private void syncContainers(Resource resource) throws IOException {
		if (resource instanceof ResourceContainer)
		{
			sync(resource);
			for (Resource res : ((ResourceContainer) resource).getResources()) {
				syncContainers(res);
			}
		}
	}

	private void syncResources(Resource resource) throws IOException {
		if (resource.getType() == ResourceType.RESOURCE) {
			sync(resource);
			return;
		}
		
		if (resource instanceof ResourceContainer) {
			for (Resource res : ((ResourceContainer) resource).getResources()) {
				syncResources(res);
			}
		}
	}

	private void update(Resource resource, RemoteResource remote) {
		resource.setId(remote.getId());
		resource.setName(remote.getName());
		resource.getStatus().setStatus(ResourceStatusType.CREATED, remote);
	}

	public static ResourceManager getInstance() {
		return SingletonHolder.INSTANCE;
	}

	private static class SingletonHolder {
		protected static final ResourceManager INSTANCE = new ResourceManager();
	}
}
