package br.com.doisxtres.alvolante.resourcemanager.model;

import java.io.Serializable;

public class User implements Serializable {
	private static final long serialVersionUID = 29396286143178903L;

	private int id = -1;
	private String name = null;

	public User() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
