package br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory;

import br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.typed.ImageThumbnailFactory;
import br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.typed.PdfThumbnailFactory;
import br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.typed.VideoThumbnailFactory;
import br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.typed.ZipThumbnailFactory;

public class DefaultThumbnailFactory extends ThumbnailFactoryDelegator {
	public DefaultThumbnailFactory() {
		super(new ThumbnailFactory[] {
				new ImageThumbnailFactory(),
				new VideoThumbnailFactory(),
				new PdfThumbnailFactory(),
				new ZipThumbnailFactory()
		});
	}
}
