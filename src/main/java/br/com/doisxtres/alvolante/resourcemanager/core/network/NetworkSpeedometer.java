package br.com.doisxtres.alvolante.resourcemanager.core.network;

import java.util.concurrent.TimeUnit;

import javafx.beans.InvalidationListener;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import br.com.doisxtres.alvolante.resourcemanager.core.ThreadPoolManager;

public class NetworkSpeedometer implements Runnable, ObservableValue<Long> {
	private long countingSpeed = 0;
	private final ObjectProperty<Long> speed = new SimpleObjectProperty<>(0L);
	
	public NetworkSpeedometer() {
		ThreadPoolManager.getInstance().scheduleAtFixedRate(this, 1, 1, TimeUnit.SECONDS);
	}
	
	public void tick(long bytes) {
		countingSpeed += bytes;
	}

	@Override
	public void run() {
		speed.set(countingSpeed);
		countingSpeed = 0;
	}

	@Override
	public void addListener(InvalidationListener listener) {
		speed.addListener(listener);
	}

	@Override
	public void removeListener(InvalidationListener listener) {
		speed.removeListener(listener);
	}

	@Override
	public void addListener(ChangeListener<? super Long> listener) {
		speed.addListener(listener);
	}

	@Override
	public void removeListener(ChangeListener<? super Long> listener) {
		speed.removeListener(listener);
	}

	@Override
	public Long getValue() {
		return speed.getValue();
	}
}
