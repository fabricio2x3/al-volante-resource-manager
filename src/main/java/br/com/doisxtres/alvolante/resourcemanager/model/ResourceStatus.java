package br.com.doisxtres.alvolante.resourcemanager.model;

public final class ResourceStatus {
	private ResourceStatusType status = ResourceStatusType.INITIAL;
	private Object details = null;

	public ResourceStatusType getStatus() {
		return status;
	}

	public void setStatus(ResourceStatusType status) {
		this.status = status;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public void setStatus(ResourceStatusType status, Object details) {
		setStatus(status);
		setDetails(details);
	}
}
