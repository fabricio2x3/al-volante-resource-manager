package br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.typed;

import java.io.File;
import java.io.IOException;

import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;

import br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.RegexThumbnailFactory;

public class VideoThumbnailFactory extends RegexThumbnailFactory {
	private final ConvertCmd cmd;
	private final IMOperation op;
	
	public VideoThumbnailFactory() {
		super("(.*(\\.(?i)(mp4|mov|3gp|mkv|avi|wmv|mpg|mpeg|m4v|3g2))$)");
		
		cmd = new ConvertCmd();
		op = new IMOperation();
		op.addImage();
		op.thumbnail(240, null);
		op.flatten();
		op.quality(80.0);
		op.addImage();
	}

	@Override
	protected File createImpl(File source) throws IOException {
		File tmp = File.createTempFile("thumbnail", ".jpg");

		try {
			cmd.run(op, source.getAbsolutePath() + "[0]", tmp.getAbsolutePath());
			return tmp;
		} catch (Exception e) {
			throw new IOException(e);
		}
	}
}
