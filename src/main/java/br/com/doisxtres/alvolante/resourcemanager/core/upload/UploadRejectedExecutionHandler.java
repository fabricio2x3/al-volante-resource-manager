package br.com.doisxtres.alvolante.resourcemanager.core.upload;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Logger;

public class UploadRejectedExecutionHandler implements RejectedExecutionHandler {
	private static final Logger LOGGER = Logger.getLogger(UploadRejectedExecutionHandler.class.getName());
	
	@Override
	public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		LOGGER.warning("Task " + r.toString() + " rejected from " + executor.toString());
	}
}
