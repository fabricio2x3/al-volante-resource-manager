package br.com.doisxtres.alvolante.resourcemanager.view.scene.control.cell;

import javafx.beans.binding.StringBinding;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import br.com.doisxtres.alvolante.resourcemanager.model.Resource;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceStatus;

public class ResourceStatusTreeTableCell extends TreeTableCell<Resource, ResourceStatus> {
	public static <S> Callback<TreeTableColumn<Resource, ResourceStatus>, TreeTableCell<Resource, ResourceStatus>> forTreeTableColumn() {
		return param -> new ResourceStatusTreeTableCell();
	}
	
	private final Label label;
	
	public ResourceStatusTreeTableCell() {
		label = new Label();
		setAlignment(Pos.CENTER);
	}
	
	@Override
	protected void updateItem(ResourceStatus item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
		} else {
			label.textProperty().unbind();
			
			final TreeTableColumn<Resource, ResourceStatus> column = getTableColumn();
			final ObservableValue<ResourceStatus> observable = column == null ? null : column.getCellObservableValue(getIndex());

			if (observable != null) {
				label.textProperty().bind(new LabelBinding(observable));
			} else if (item != null) {
				updateLabelStyle(item);
				label.setText(item.getStatus().name());
			}

			setGraphic(label);
		}
	}

	protected void updateLabelStyle(ResourceStatus status) {
		switch (status.getStatus()) {
			case ERROR:
				label.setTextFill(Color.RED);
				break;
			case UPLOAD_SUSPENDED:
				label.setTextFill(Color.ORANGE);
				break;
			case UPLOADING:
			case UPLOADED:
			case GENERATING_THUMBNAIL:
			case UPLOADING_THUMBNAIL:
			case FINISHING:
			case FINISHED:
				label.setTextFill(Color.GREEN);
				break;
			default:
				label.setTextFill(null);
		}
	}
	
	protected String getLabelForStatus(ResourceStatus status) {
		switch (status.getStatus()) {
			case ERROR: {
				if (status.getDetails() instanceof Exception) {
					Exception details = (Exception) status.getDetails();
					return details.getLocalizedMessage();
				}
				break;
			}
			default:
				break;
		}
		return status.getStatus().name();
	}
	
	protected class LabelBinding extends StringBinding {
		private final ObservableValue<ResourceStatus> observable;
		
		public LabelBinding(ObservableValue<ResourceStatus> observable) {
			this.observable = observable;
			bind(this.observable);
		}
		
		@Override
		protected String computeValue() {
			ResourceStatus status = observable.getValue();
			updateLabelStyle(status);
			return getLabelForStatus(status);
		}
	}
}
