package br.com.doisxtres.alvolante.resourcemanager.core;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class ThreadPoolManager {
	private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(0);
	
	protected ThreadPoolManager() {
		
	}

	public void shutdown() {
		executorService.shutdownNow();
	}
	
	public void execute(Runnable command) {
		executorService.execute(command);
	}

	public <T> void executeAsync(Callable<T> callable, AsyncCallback<T> callback) {
		executorService.execute(() -> {
			try {
				callback.completed(callable.call());
			} catch (Exception e) {
				callback.failed(e);
			}
		});
	}

	public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
		return executorService.scheduleAtFixedRate(command, initialDelay, period, unit);
	}

	public static ThreadPoolManager getInstance() {
		return SingletonHolder.INSTANCE;
	}

	private static class SingletonHolder {
		protected static final ThreadPoolManager INSTANCE = new ThreadPoolManager();
	}
}
