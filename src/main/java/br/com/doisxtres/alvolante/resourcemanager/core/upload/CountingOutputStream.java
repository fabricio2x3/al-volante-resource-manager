package br.com.doisxtres.alvolante.resourcemanager.core.upload;

import java.io.IOException;
import java.io.OutputStream;

import br.com.doisxtres.alvolante.resourcemanager.core.network.NetworkService;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class CountingOutputStream extends OutputStream {
	private ObjectProperty<Long> count = new SimpleObjectProperty<>(0L);
	private final OutputStream out;
	
	public CountingOutputStream(OutputStream out, long initialCount) {
		this.out = out;
		this.count.set(initialCount);
	}

	public CountingOutputStream(OutputStream out) {
		this.out = out;
	}

	@Override
	public void write(int b) throws IOException {
		out.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException {
		out.write(b);
		incrementCount(b.length);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		out.write(b, off, len);
		incrementCount(len);
	}

	@Override
	public void flush() throws IOException {
		out.flush();
	}

	@Override
	public void close() throws IOException {
		out.close();
	}

	private void incrementCount(long n) {
		Platform.runLater(() -> count.set(count.get() + n));
		NetworkService.getInstance().getUploadSpeedometer().tick(n);
	}

	public ObjectProperty<Long> countProperty() {
		return count;
	}
}
