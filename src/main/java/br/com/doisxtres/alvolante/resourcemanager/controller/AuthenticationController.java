package br.com.doisxtres.alvolante.resourcemanager.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import br.com.doisxtres.alvolante.resourcemanager.core.AsyncCallback;
import br.com.doisxtres.alvolante.resourcemanager.core.AuthenticationManager;
import br.com.doisxtres.alvolante.resourcemanager.model.User;

public class AuthenticationController {
	public static final String VIEW_NAME = "Authentication.fxml";

	@FXML
	private TextField usernameField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private Button loginButton;

	@FXML
	private Label errorLabel;

	@FXML
	private void initialize() {
		// TODO Remove this
		//usernameField.setText("alvolanteadmin");
		//passwordField.setText("@alvolante123@");
	}

	@FXML
	private void onKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (usernameField.getText().isEmpty()) {
				usernameField.requestFocus();
				return;
			}
			if (passwordField.getText().isEmpty()) {
				passwordField.requestFocus();
				return;
			}

			loginButton.fireEvent(new ActionEvent(event.getSource(), event.getTarget()));
		}
	}

	@FXML
	private void onLoginButtonAction(ActionEvent event) {
		String username = usernameField.getText();
		String password = passwordField.getText();
		
		if (username.isEmpty()) {
			usernameField.requestFocus();
			return;
		}
		if (password.isEmpty()) {
			passwordField.requestFocus();
			return;
		}

		// Disable the button to prevent re-submit
		loginButton.setDisable(true);
		usernameField.setDisable(true);
		passwordField.setDisable(true);
		// Hide the error label
		errorLabel.setVisible(false);

		// Validate the username and password
		AuthenticationManager.getInstance().login(username, password, new AsyncCallback<User>() {
			@Override
			public void completed(User response) { }

			@Override
			public void failed(Exception cause) {
				Platform.runLater(() -> {
					// Show the error
					errorLabel.setText(cause.getLocalizedMessage());
					errorLabel.setVisible(true);

					// Enable the submit button
					loginButton.setDisable(false);
					usernameField.setDisable(false);
					passwordField.setDisable(false);
					// Clear the password for security
					passwordField.clear();
				});
			}
		});
	}
}
