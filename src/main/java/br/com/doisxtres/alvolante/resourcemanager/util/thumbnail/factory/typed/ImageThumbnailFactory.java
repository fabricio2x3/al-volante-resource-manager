package br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.typed;

import java.io.File;
import java.io.IOException;

import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;

import br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.RegexThumbnailFactory;

public class ImageThumbnailFactory extends RegexThumbnailFactory {
	private final ConvertCmd cmd;
	private final IMOperation op;

	public ImageThumbnailFactory() {
		super("(.*(\\.(?i)(jpg|png|gif|bmp|jpeg|eps|ai|psd))$)");

		cmd = new ConvertCmd();
		op = new IMOperation();
		op.addImage();
		op.thumbnail(240, null);
		op.flatten();
		op.quality(80.0);
		op.addImage();
	}

	@Override
	protected File createImpl(File source) throws IOException {
		File tmp = File.createTempFile("thumbnail", ".jpg");

		try {
			cmd.run(op, source.getAbsolutePath(), tmp.getAbsolutePath());
			return tmp;
		} catch (Exception e) {
			throw new IOException(e);
		}
	}
}
