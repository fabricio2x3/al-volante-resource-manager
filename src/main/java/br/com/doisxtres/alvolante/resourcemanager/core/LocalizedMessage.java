package br.com.doisxtres.alvolante.resourcemanager.core;

import br.com.doisxtres.alvolante.resourcemanager.ApplicationMain;

public final class LocalizedMessage {
	private final String key;
	private final Object[] args;
	
	public LocalizedMessage(String key, Object... args) {
		if (key == null) {
			throw new NullPointerException("Key must not be null");
		}
		this.key = key;
		this.args = args;
	}

	@Override
	public String toString() {
		String message = null;
		try {
			message = ApplicationMain.getMessage(key, args);
		} catch (Exception e) { }
		
		if (message == null) {
			StringBuilder sb = new StringBuilder();
			sb.append(key);
			
			if (args.length > 0) {
				sb.append(" [");
				final String argSeparator = ", ";
				for (Object arg : args) {
					sb.append(arg);
					sb.append(argSeparator);
				}
				sb.delete(sb.length() - argSeparator.length(), sb.length());
				sb.append("]");
			}
			
			message = sb.toString();
		}
		
		return message;
	}
}
