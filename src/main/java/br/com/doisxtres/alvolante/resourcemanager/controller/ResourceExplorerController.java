package br.com.doisxtres.alvolante.resourcemanager.controller;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.stage.DirectoryChooser;
import br.com.doisxtres.alvolante.resourcemanager.core.dialog.ExceptionAlert;
import br.com.doisxtres.alvolante.resourcemanager.model.Resource;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceContainer;
import br.com.doisxtres.alvolante.resourcemanager.util.ResourceUtils;

public class ResourceExplorerController {
	private static final Logger LOGGER = Logger.getLogger(ResourceUploaderController.class.getName());

	public static final String VIEW_NAME = "ResourceExplorer.fxml";

	@FXML
	private TextField directoryField;
	@FXML
	private Button selectFilesButton;

	@FXML
	private TreeTableView<Resource> resourcesTable;
	@FXML
	private TreeTableColumn<Resource, String> resourceNameColumn;

	@FXML
	private Button cancelButton;
	@FXML
	private Button confirmButton;

	private ResourceContainer pages;
	private ObjectProperty<ResourceContainer> pagesProperty = new SimpleObjectProperty<>();

	@FXML
	private void initialize() {
		directoryField.textProperty().addListener((observable, oldValue, newValue) -> onDirectoryFieldChanged(observable, oldValue, newValue));

		resourceNameColumn.setCellValueFactory(param -> param.getValue().getValue().getNameProperty());
	}

	private void onDirectoryFieldChanged(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		try {
			pages = ResourceUtils.loadPages(new File(newValue));
			TreeItem<Resource> root = buildTree(pages);
			resourcesTable.setRoot(root);
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Failed to load resouces from disk", e);
			
			final Alert alert = new ExceptionAlert(e);
			alert.showAndWait();
		}
	}

	@FXML
	private void onSelectFilesButtonAction(ActionEvent event) {
		final DirectoryChooser directoryChooser = new DirectoryChooser();
		directoryChooser.setTitle("Open Resource File");
		final File directory = directoryChooser.showDialog(((Node) event.getSource()).getScene().getWindow());
		if (directory != null) {
			directoryField.setText(directory.getAbsolutePath());
		}
	}

	@FXML
	private void onCancelButtonAction(ActionEvent event) {
		((Node) event.getSource()).getScene().getWindow().hide();
	}

	@FXML
	private void onConfirmButtonAction(ActionEvent event) {
		pagesProperty.set(pages);
		onCancelButtonAction(event);
	}

	public ObjectProperty<ResourceContainer> pagesProperty() {
		return pagesProperty;
	}

	public static TreeItem<Resource> buildTree(ResourceContainer pages) {
		TreeItem<Resource> pagesItem = new TreeItem<>(pages);
		pagesItem.setExpanded(true);

		for (Resource page : pages.getResources()) {
			TreeItem<Resource> pageItem = new TreeItem<>(page);
			pageItem.setExpanded(true);

			for (Resource subpage : ((ResourceContainer) page).getResources()) {
				TreeItem<Resource> subpageItem = new TreeItem<>(subpage);
				subpageItem.setExpanded(true);

				for (Resource section : ((ResourceContainer) subpage).getResources()) {
					TreeItem<Resource> sectionItem = new TreeItem<>(section);

					for (Resource resource : ((ResourceContainer) section).getResources()) {
						TreeItem<Resource> resourceItem = new TreeItem<>(resource);
						sectionItem.getChildren().add(resourceItem);
					}

					subpageItem.getChildren().add(sectionItem);
				}

				pageItem.getChildren().add(subpageItem);
			}

			pagesItem.getChildren().add(pageItem);
		}

		return pagesItem;
	}
}
