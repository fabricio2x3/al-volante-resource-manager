package br.com.doisxtres.alvolante.resourcemanager.core.upload;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;

import org.apache.http.client.HttpResponseException;
import org.apache.http.entity.mime.content.ContentBody;

import br.com.doisxtres.alvolante.resourcemanager.Config;
import br.com.doisxtres.alvolante.resourcemanager.core.FileSplitter;
import br.com.doisxtres.alvolante.resourcemanager.core.network.NetworkService;
import br.com.doisxtres.alvolante.resourcemanager.core.network.ResourceFileBody;
import br.com.doisxtres.alvolante.resourcemanager.model.Resource;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceStatusType;
import br.com.doisxtres.alvolante.resourcemanager.util.Utils;
import br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.DefaultThumbnailFactory;
import br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.ThumbnailFactory;

public class ResourceUploadTask implements Runnable, Comparable<ResourceUploadTask> {
	private static final Logger LOGGER = Logger.getLogger(ResourceUploadTask.class.getName());

	private final Resource resource;
	private int priority = Thread.NORM_PRIORITY;

	private String remoteFileName;
	private String remoteThumbnailName;

	public ResourceUploadTask(Resource resource) {
		this.resource = resource;
	}

	@Override
	public void run() {
		try {
			if (Config.isMultipartUploadEnabled() && resource.getFile().length() >= Config.getMultipartUploadMinFileSize()) {
				multipartUploadResource();
			} else {
				uploadResource();
			}
			generateAndUploadFileArtifacts(new DefaultThumbnailFactory());
			finish();
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Upload finished with errors: \"" + resource.toString() + "\"", e);
			resource.getStatus().setStatus(ResourceStatusType.ERROR, e);
		}
	}

	private void uploadResource() throws Exception {
		LOGGER.info("Upload started: " + resource.toString());
		Platform.runLater(() -> resource.setUploadStart(new Date()));
		
		UploadResponse upload = tryUntilSucceeds(() -> {
			resource.getStatus().setStatus(ResourceStatusType.UPLOADING, this);
			// Try to upload the part
			UploadResponse response = NetworkService.getInstance().upload(resource);
			
			// Check if the file was successfully uploaded
			if (response.getStatus() == 1) {
				return response;
			}
			throw new HttpResponseException(500, "Upload failed");
		},
		// Failed ;-(
		() -> {
			// Reset the uploaded size
			resource.getUploadedSizeProperty().unbind();
			resource.setUploadedSize(0);

			// Notify the problem and wait before trying again
			resource.getStatus().setStatus(ResourceStatusType.UPLOAD_SUSPENDED, this);
		});
		
		Platform.runLater(() -> resource.setUploadEnd(new Date()));
		resource.getStatus().setStatus(ResourceStatusType.UPLOADED, upload);
		remoteFileName = upload.getFile();

		LOGGER.info("Upload finished: " + resource.toString());
	}

	private void multipartUploadResource() throws Exception {
		final File file = resource.getFile();

		// Split the file
		try (FileSplitter splitter = new FileSplitter(file, Config.getMultipartUploadPartSize())) {
			LOGGER.info(getClass().getSimpleName() + ": Initiating the upload of \"" + resource.toString() + "\"...");
			Platform.runLater(() -> resource.setUploadStart(new Date()));
			
			// Initiate the upload
			final MultipartUploadIdentifier identifier = tryUntilSucceeds(() -> NetworkService.getInstance().initiateMultipartUpload(file.getName()));
			final String tmpPath = identifier.getTmpPath();
			final String uploadId = identifier.getUploadId();
			final List<String> partIdentifiers = new ArrayList<>();

			long uploadedSize = 0;
			int partNumber = 1;

			// Go through all the parts
			while (splitter.hasNext()) {
				final File part = splitter.next();
				final long currentUploadedSize = uploadedSize;
				final int currentPartNumber = partNumber;
				
				LOGGER.info(getClass().getSimpleName() + ": Part " + partNumber + "/" + splitter.getNumberOfParts() + " (" + Utils.formatFileSize(part.length()) + ") of \"" + resource.toString() + "\" initiated");

				// Try to upload the part until it succeeds
				String partIdentifier = tryUntilSucceeds(() -> {
					resource.getStatus().setStatus(ResourceStatusType.UPLOADING, this);
					// Try to upload the part
					final ContentBody contentBody = new ResourceFileBody(resource, part, currentUploadedSize);
					return NetworkService.getInstance().uploadPart(tmpPath, uploadId, currentPartNumber, contentBody);
				},
				// Failed ;-(
				() -> {
					// Reset the uploaded size
					Platform.runLater(() -> {
						resource.getUploadedSizeProperty().unbind();
						resource.setUploadedSize(currentUploadedSize);
					});

					// Notify the problem and wait before trying again
					resource.getStatus().setStatus(ResourceStatusType.UPLOAD_SUSPENDED, this);
				});
				
				// Part successfully uploaded
				partIdentifiers.add(partIdentifier);
				uploadedSize += part.length();
				LOGGER.info(getClass().getSimpleName() + ": Part " + partNumber + "/" + splitter.getNumberOfParts() + " of \"" + resource.toString() + "\" successfully uploaded");

				// Move to the next part
				partNumber++;
			}

			// Complete the upload
			LOGGER.info(getClass().getSimpleName() + ": Completing the upload of \"" + resource.toString() + "\"...");
			final String remoteFileName = tryUntilSucceeds(() -> NetworkService.getInstance().completeMultipartUpload(tmpPath, uploadId, partIdentifiers));

			Platform.runLater(() -> resource.setUploadEnd(new Date()));
			resource.getStatus().setStatus(ResourceStatusType.UPLOADED, this);
			this.remoteFileName = remoteFileName;

			LOGGER.info("Upload completed: \"" + resource.toString() + "\"");
		}
	}
	
	private <T> T tryUntilSucceeds(Callable<T> callable) throws Exception {
		return tryUntilSucceeds(callable, null);
	}
	
	private <T> T tryUntilSucceeds(Callable<T> callable, Runnable onTryFail) throws Exception {
		while (true) {
			try {
				try {
					return callable.call();
				} catch (SocketException e) {
					// Notify network problems
					NetworkService.getInstance().networkProblemsProperty().add(this);
				} catch (HttpResponseException e) {
					LOGGER.log(Level.WARNING, "Resource \"" + resource + "\" error -> " + e.getStatusCode() + ": " + e.getLocalizedMessage(), e);
				} catch (IOException e) {
					// TODO Check if should retry if caught here or it will fail over and over again
					LOGGER.log(Level.WARNING,  "Resource \"" + resource + "\" error -> " + e.getLocalizedMessage(), e);
				}

				if (onTryFail != null) {
					onTryFail.run();
				}

				try {
					Thread.sleep(Config.getUploadRetryInterval());
				} catch (InterruptedException e) {
					// Just ignore
				}
			} finally {
				// Cleanup network problems in this task
				NetworkService.getInstance().networkProblemsProperty().remove(this);
			}
		}
	}

	@SuppressWarnings("unused")
	private void generateAndUploadThumbnail() {
		final File file = resource.getFile();

		final ThumbnailFactory thumbnailFactory = new DefaultThumbnailFactory();
		if (thumbnailFactory.accept(file)) {
			resource.getStatus().setStatus(ResourceStatusType.GENERATING_THUMBNAIL, this);

			try {
				File thumbnail = thumbnailFactory.create(file);
				if (thumbnail != null) {
					resource.getStatus().setStatus(ResourceStatusType.UPLOADING_THUMBNAIL, thumbnail);

					UploadResponse upload = NetworkService.getInstance().upload(thumbnail);
					if (upload.getStatus() == 1) {
						remoteThumbnailName = upload.getFile();
					}
				}
			} catch (IOException e) {
				// Just log this exception
				LOGGER.log(Level.WARNING, "Error while generating/uploading thumbnail: \"" + resource.toString() + "\"", e);
			}
		}
	}
	
	private void generateAndUploadFileArtifacts(ThumbnailFactory factory) {
		final File file = resource.getFile();
		
		if (factory.accept(file)) {
			resource.getStatus().setStatus(ResourceStatusType.GENERATING_THUMBNAIL, this);

			try {
				File thumbnail = factory.create(file);
				if (thumbnail != null) {
					resource.getStatus().setStatus(ResourceStatusType.UPLOADING_THUMBNAIL, thumbnail);

					UploadResponse upload = NetworkService.getInstance().upload(thumbnail);
					if (upload.getStatus() == 1) {
						remoteThumbnailName = upload.getFile();
					}
				}
			} catch (IOException e) {
				// Just log this exception
				LOGGER.log(Level.WARNING, "Error while generating/uploading thumbnail: \"" + resource.toString() + "\"", e);
			}
		}
	}

	private void finish() {
		resource.getStatus().setStatus(ResourceStatusType.FINISHING, this);
		try {
			NetworkService.getInstance().updateResource(resource.getId(), remoteFileName, remoteThumbnailName);
			resource.getStatus().setStatus(ResourceStatusType.FINISHED, this);
		} catch (IOException e) {
			resource.getStatus().setStatus(ResourceStatusType.ERROR, e);
			LOGGER.log(Level.WARNING, "Error while finishing resource: \"" + resource.toString() + "\"", e);
		}
	}

	@Override
	public int compareTo(ResourceUploadTask o) {
		int diff = priority - o.priority;
		if (diff == 0) {
			diff = resource.compareTo(o.getResource());
		}
		return diff;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Resource getResource() {
		return resource;
	}
}
