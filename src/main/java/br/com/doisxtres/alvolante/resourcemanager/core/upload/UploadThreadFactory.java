package br.com.doisxtres.alvolante.resourcemanager.core.upload;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class UploadThreadFactory implements ThreadFactory {
	private final AtomicInteger threadNumber = new AtomicInteger(1);
	private final ThreadGroup group = new ThreadGroup("Upload");
	private final String namePrefix = "UploadThread-";
	
	@Override
	public Thread newThread(Runnable r) {
		Thread thread = new Thread(group, r, nextName());
		if (thread.isDaemon()) {
			thread.setDaemon(false);
		}
		if (thread.getPriority() != Thread.NORM_PRIORITY) {
			thread.setPriority(Thread.NORM_PRIORITY);
		}
		return thread;
	}
	
	private String nextName() {
		return namePrefix + threadNumber.getAndIncrement();
	}
}
