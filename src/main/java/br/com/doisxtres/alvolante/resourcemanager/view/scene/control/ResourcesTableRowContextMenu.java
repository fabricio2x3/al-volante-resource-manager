package br.com.doisxtres.alvolante.resourcemanager.view.scene.control;

import br.com.doisxtres.alvolante.resourcemanager.ApplicationMain;
import br.com.doisxtres.alvolante.resourcemanager.core.upload.UploadTaskManager;
import br.com.doisxtres.alvolante.resourcemanager.model.Resource;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeTableRow;

public class ResourcesTableRowContextMenu extends ContextMenu {
	private final TreeTableRow<Resource> row;

	public ResourcesTableRowContextMenu(TreeTableRow<Resource> row) {
		this.row = row;

		getItems().add(buildPriorityMenu());
	}
	
	public TreeTableRow<Resource> getRow() {
		return row;
	}
	
	private Menu buildPriorityMenu() {
		final Menu priority = new Menu(ApplicationMain.getMessage("resource.priority"));
		
		final RadioMenuItem priorityHigh = new RadioMenuItem(ApplicationMain.getMessage("reosurce.priority.high"));
		final RadioMenuItem priorityNormal = new RadioMenuItem(ApplicationMain.getMessage("resource.priority.normal"));
		final RadioMenuItem priorityLow = new RadioMenuItem(ApplicationMain.getMessage("resource.priority.low"));
		
		final ToggleGroup priorityToggleGroup = new ToggleGroup();
		priorityHigh.setToggleGroup(priorityToggleGroup);
		priorityNormal.setToggleGroup(priorityToggleGroup);
		priorityLow.setToggleGroup(priorityToggleGroup);
		
		priorityNormal.setSelected(true);
		
		priorityHigh.setOnAction((event) -> setPriority(Thread.MAX_PRIORITY));
		priorityNormal.setOnAction((event) -> setPriority(Thread.NORM_PRIORITY));
		priorityLow.setOnAction((event) -> setPriority(Thread.MIN_PRIORITY));
		
		priority.getItems().add(priorityHigh);
		priority.getItems().add(priorityNormal);
		priority.getItems().add(priorityLow);
		
		return priority;
	}
	
	private void setPriority(int priority) {
		UploadTaskManager.getInstance().setPriority(row.getTreeItem().getValue(), priority);
	}
}
