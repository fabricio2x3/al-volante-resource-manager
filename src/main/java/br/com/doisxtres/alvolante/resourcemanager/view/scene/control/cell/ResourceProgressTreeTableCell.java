package br.com.doisxtres.alvolante.resourcemanager.view.scene.control.cell;

import javafx.beans.binding.ObjectBinding;
import javafx.beans.binding.StringBinding;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;
import br.com.doisxtres.alvolante.resourcemanager.model.Resource;
import br.com.doisxtres.alvolante.resourcemanager.util.Utils;

public class ResourceProgressTreeTableCell extends TreeTableCell<Resource, Resource> {
	public static <S> Callback<TreeTableColumn<Resource, Resource>, TreeTableCell<Resource, Resource>> forTreeTableColumn() {
		return param -> new ResourceProgressTreeTableCell();
	}
	
	private final StackPane pane;
	private final ProgressBar progressBar;
	private final Label label;
	
	public ResourceProgressTreeTableCell() {
		getStyleClass().add("progress-bar-tree-table-cell");

		progressBar = new ProgressBar();
		progressBar.setMaxWidth(Double.MAX_VALUE);
		progressBar.setPrefHeight(24);
		label = new Label();
		pane = new StackPane(progressBar, label);
	}
	
	@Override
	protected void updateItem(Resource item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
		} else {
			progressBar.progressProperty().unbind();
			label.textProperty().unbind();

			progressBar.progressProperty().bind(new ProgressBinding(item));
			label.textProperty().bind(new LabelBinding(item));

			setGraphic(pane);
		}
	}
	
	protected static class ProgressBinding extends ObjectBinding<Double> {
		private final Resource resource;
		
		public ProgressBinding(Resource resource) {
			this.resource = resource;
			bind(resource.getSizeProperty(), resource.getUploadedSizeProperty());
		}
		
		@Override
		protected Double computeValue() {
			return resource.getProgress();
		}
	}
	
	protected static class LabelBinding extends StringBinding {
		private final Resource resource;
		
		public LabelBinding(Resource resource) {
			this.resource = resource;
			bind(resource.getSizeProperty(), resource.getUploadedSizeProperty());
		}

		@Override
		protected String computeValue() {
//			if (resource.getUploadStart() == null) {
//				return ResourceManagerMain.getMessage("resource.upload.queued");
//			}
			Long size = resource.getSize();
			Long uploadedSize = resource.getUploadedSize();
			return Utils.formatFileSize(uploadedSize) + "/" + Utils.formatFileSize(size);
		}
	}
}
