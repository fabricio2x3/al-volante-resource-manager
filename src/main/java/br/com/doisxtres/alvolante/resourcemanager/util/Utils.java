package br.com.doisxtres.alvolante.resourcemanager.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Utils {
	private Utils() { }
	
	private static final DateFormat DATETIME_FORMATTER = new SimpleDateFormat("dd/MM/yyyy HH:mm"); 
	private static final NumberFormat PERCENT_FORMATTER = new DecimalFormat("#.#%");
	private static final NumberFormat FILE_SIZE_FORMATTER = new DecimalFormat("#,##0.#");
	private static final String[] BYTE_UNITS = new String[] { "b", "kb", "mb", "gb", "tb" };
	
	public static String formatDateTime(Date date) {
		return date == null ? null : DATETIME_FORMATTER.format(date);
	}
	
	public static String formatPercent(double val) {
		return PERCENT_FORMATTER.format(val);
	}
	
	public static String formatFileSize(long size) {
		if (size <= 0) {
			return "0" + BYTE_UNITS[0];
		}
		
		int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
		return FILE_SIZE_FORMATTER.format(size / Math.pow(1024, digitGroups)) + BYTE_UNITS[digitGroups];
	}
	
	public static String formatDuration(long duration) {
		int seconds = (int) ((duration / 1000) % 60);
		int minutes = (int) ((duration / (1000 * 60)) % 60);
		int hours = (int) ((duration / (1000 * 60 * 60)) % 24);
		int days = (int) (duration / (1000 * 60 * 60 * 24));
		
		StringBuilder sb = new StringBuilder();
		if (days > 0) {
			sb.append(days);
			sb.append("d ");
		}
		if (hours > 0) {
			sb.append(hours);
			sb.append("h ");
		}
		if (minutes > 0) {
			sb.append(minutes);
			sb.append("m ");
		}
		sb.append(seconds);
		sb.append("s");
		
		return sb.toString();
	}
}
