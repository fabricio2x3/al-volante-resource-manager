package br.com.doisxtres.alvolante.resourcemanager.core.upload;

import java.io.Serializable;

public class UploadResponse implements Serializable {
	private static final long serialVersionUID = 7513514097203313045L;
	
	private int status;
	private String file;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
}
