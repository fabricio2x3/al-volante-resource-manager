package br.com.doisxtres.alvolante.resourcemanager.controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import br.com.doisxtres.alvolante.resourcemanager.ApplicationMain;
import br.com.doisxtres.alvolante.resourcemanager.core.AuthenticationManager;
import br.com.doisxtres.alvolante.resourcemanager.model.User;

public class ApplicationController {
	private static final Logger LOGGER = Logger.getLogger(ApplicationController.class.getName());

	public static final String VIEW_NAME = "Application.fxml";

	@FXML
	private BorderPane container;

	@FXML
	private void initialize() {
		onLoggedUserChanged(AuthenticationManager.getInstance().getLoggedUser());
		AuthenticationManager.getInstance().addListener((observable, oldValue, newValue) -> onLoggedUserChanged(newValue));
	}

	public void changeView(Node view) {
		container.setCenter(view);
	}

	private void onLoggedUserChanged(User loggedUser) {
		try {
			final Node view;
			if (loggedUser == null) {
				view = ApplicationMain.loadView(AuthenticationController.VIEW_NAME);
			} else {
				view = ApplicationMain.loadView(ResourceUploaderController.VIEW_NAME);
			}
			changeView(view);
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
