package br.com.doisxtres.alvolante.resourcemanager.model;

public enum ResourceType {
	ROOT,
	PAGE,
	SUBPAGE,
	SECTION,
	RESOURCE
}
