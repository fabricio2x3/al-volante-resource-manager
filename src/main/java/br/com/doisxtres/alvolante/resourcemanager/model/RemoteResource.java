package br.com.doisxtres.alvolante.resourcemanager.model;

public class RemoteResource {
	private Integer id = null;
	private String name = null;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
