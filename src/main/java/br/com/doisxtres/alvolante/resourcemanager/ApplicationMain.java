package br.com.doisxtres.alvolante.resourcemanager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import br.com.doisxtres.alvolante.resourcemanager.controller.ApplicationController;
import br.com.doisxtres.alvolante.resourcemanager.core.ThreadPoolManager;
import br.com.doisxtres.alvolante.resourcemanager.core.network.NetworkService;
import br.com.doisxtres.alvolante.resourcemanager.core.upload.UploadTaskManager;

public class ApplicationMain extends Application {
	private static final Logger LOGGER = Logger.getLogger(ApplicationMain.class.getName());

	public static final String VIEWS_BASE_PATH = "view/";
	public static final String MESSAGES_BUNDLE_BASE_NAME = ApplicationMain.class.getPackage().getName() + ".i18n.Messages";

	private static ResourceBundle messages = null;

	private Stage primaryStage;
	private BorderPane rootLayout;

	public static void main(String[] args) {
		// Setup logging
		new File("logs").mkdirs();
		try (InputStream is = ApplicationMain.class.getResourceAsStream("/logging.properties")) {
			LogManager.getLogManager().readConfiguration(is);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Failed to load logging configuration from file", e);
		}

		// Initialize the locale with the system default
		setLocale(Locale.getDefault());

		// Launch the application
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle(getMessage("application.title", getVersion()));

		try {
			rootLayout = loadView(ApplicationController.VIEW_NAME);

			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		UploadTaskManager.getInstance().shutdown();
		NetworkService.getInstance().shutdown();
		ThreadPoolManager.getInstance().shutdown();
	}

	public static String getVersion() {
		String version = ApplicationMain.class.getPackage().getImplementationVersion();
		if (version == null) {
			version = "Undefined";
		}
		return version;
	}

	public static void setLocale(Locale locale) {
		messages = ResourceBundle.getBundle(MESSAGES_BUNDLE_BASE_NAME, locale, ApplicationMain.class.getClassLoader());
	}

	public static String getMessage(String key) {
		return messages.getString(key);
	}

	public static String getMessage(String key, Object... args) {
		return String.format(messages.getString(key), args);
	}

	public static <T> T loadView(String name) throws IOException {
		return createViewLoader(name).load();
	}

	public static FXMLLoader createViewLoader(String name) {
		FXMLLoader loader = new FXMLLoader();
		loader.setResources(messages);
		loader.setLocation(getView(name));
		return loader;
	}

	public static URL getView(String name) {
		return ApplicationMain.class.getResource(VIEWS_BASE_PATH + name);
	}

	public static Stage createModal(Parent root, Window owner) {
		Stage stage = new Stage();
		stage.setScene(new Scene(root));
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initOwner(owner);
		return stage;
	}
}
