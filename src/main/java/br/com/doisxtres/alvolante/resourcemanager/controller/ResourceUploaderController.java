package br.com.doisxtres.alvolante.resourcemanager.controller;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.SetChangeListener.Change;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableRow;
import javafx.scene.control.TreeTableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import br.com.doisxtres.alvolante.resourcemanager.ApplicationMain;
import br.com.doisxtres.alvolante.resourcemanager.core.AuthenticationManager;
import br.com.doisxtres.alvolante.resourcemanager.core.ResourceManager;
import br.com.doisxtres.alvolante.resourcemanager.core.ThreadPoolManager;
import br.com.doisxtres.alvolante.resourcemanager.core.dialog.ExceptionAlert;
import br.com.doisxtres.alvolante.resourcemanager.core.network.NetworkService;
import br.com.doisxtres.alvolante.resourcemanager.core.upload.UploadTaskManager;
import br.com.doisxtres.alvolante.resourcemanager.model.Resource;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceContainer;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceStatus;
import br.com.doisxtres.alvolante.resourcemanager.util.ResourceUtils;
import br.com.doisxtres.alvolante.resourcemanager.util.Utils;
import br.com.doisxtres.alvolante.resourcemanager.view.scene.control.ResourcesTableRowContextMenu;
import br.com.doisxtres.alvolante.resourcemanager.view.scene.control.cell.DateTreeTableCell;
import br.com.doisxtres.alvolante.resourcemanager.view.scene.control.cell.ResourceProgressTreeTableCell;
import br.com.doisxtres.alvolante.resourcemanager.view.scene.control.cell.ResourceStatusTreeTableCell;

public class ResourceUploaderController {
	private static final Logger LOGGER = Logger.getLogger(ResourceUploaderController.class.getName());

	public static final String VIEW_NAME = "ResourceUploader.fxml";

	private static final Image CONNECTION_OK_IMAGE = new Image(ApplicationMain.getView("images/connection-ok.png").toExternalForm());
	private static final Image CONNECTION_ERROR_IMAGE = new Image(ApplicationMain.getView("images/connection-error.png").toExternalForm());

	private ResourceContainer pages = new ResourceContainer(ApplicationMain.getMessage("resource.pages"));

	@FXML
	private Label usernameLabel;
	@FXML
	private Button logoutButton;
	@FXML
	private Button selectFilesButton;

	@FXML
	private TreeTableView<Resource> resourcesTable;
	@FXML
	private TreeTableColumn<Resource, String> resourceNameColumn;
	@FXML
	private TreeTableColumn<Resource, Resource> resourceProgressColumn;
	@FXML
	private TreeTableColumn<Resource, Date> resourceUploadStartColumn;
	@FXML
	private TreeTableColumn<Resource, Date> resourceUploadEndColumn;
	@FXML
	private TreeTableColumn<Resource, String> resourceIntegrityColumn;
	@FXML
	private TreeTableColumn<Resource, ResourceStatus> resourceStatusColumn;

	@FXML
	private TextField concurrentUploadsField;
	@FXML
	private Label uploadSpeedLabel;
	@FXML
	private Label uploadRemainTimeLabel;
	@FXML
	private Label connectionStatusLabel;

	@FXML
	private void initialize() {
		usernameLabel.setText(AuthenticationManager.getInstance().getLoggedUser().getName());

		resourceUploadStartColumn.setCellFactory(DateTreeTableCell.forTreeTableColumn());
		resourceUploadEndColumn.setCellFactory(DateTreeTableCell.forTreeTableColumn());
		resourceProgressColumn.setCellFactory(ResourceProgressTreeTableCell.forTreeTableColumn());
		resourceStatusColumn.setCellFactory(ResourceStatusTreeTableCell.forTreeTableColumn());

		resourceNameColumn.setCellValueFactory(param -> param.getValue().getValue().getNameProperty());
		resourceUploadStartColumn.setCellValueFactory(param -> param.getValue().getValue().getUploadStartProperty());
		resourceUploadEndColumn.setCellValueFactory(param -> param.getValue().getValue().getUploadEndProperty());
		resourceProgressColumn.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue().getValue()));
		resourceStatusColumn.setCellValueFactory(param -> param.getValue().getValue().statusProperty());

		resourceIntegrityColumn.setCellValueFactory(param -> param.getValue().getValue().integrityBindind());

		resourcesTable.setRowFactory(param -> {
			final TreeTableRow<Resource> row = new TreeTableRow<>();

			ContextMenu menu = new ResourcesTableRowContextMenu(row);
			row.setContextMenu(menu);

			return row;
		});

		concurrentUploadsField.textProperty().addListener((observable, oldValue, newValue) -> onConcurrentUploadsFieldChanged(observable, oldValue, newValue));

		onConnectionProblemsChanged(Collections.emptySet());
		NetworkService.getInstance().networkProblemsProperty().addListener((Change<? extends Runnable> change) -> onConnectionProblemsChanged(change.getSet()));
		NetworkService.getInstance().getUploadSpeedometer().addListener((observable, oldValue, newValue) -> onUploadSpeedChanged(newValue));
	}

	@FXML
	private void onSelectFilesButtonAction(ActionEvent event) {
		try {
			FXMLLoader loader = ApplicationMain.createViewLoader(ResourceExplorerController.VIEW_NAME);
			Parent root = loader.load();
			ResourceExplorerController controller = loader.getController();
			controller.pagesProperty().addListener((observable, oldValue, newValue) -> load(newValue));

			Stage stage = ApplicationMain.createModal(root, ((Node) event.getSource()).getScene().getWindow());
			stage.show();
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}

	@FXML
	private void onLogoutButtonAction(ActionEvent event) {
		AuthenticationManager.getInstance().logout();
	}

	private void onConcurrentUploadsFieldChanged(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		int concurrentUploads = 1;
		try {
			concurrentUploads = Integer.parseInt(newValue);
		} catch (NumberFormatException e) {
			try {
				concurrentUploads = Integer.parseInt(oldValue);
			} catch (NumberFormatException e1) { }
		}

		if (concurrentUploads < 1) {
			concurrentUploads = 1;
		}
		if (concurrentUploads > UploadTaskManager.MAX_CONCURRENT_UPLOADS) {
			concurrentUploads = UploadTaskManager.MAX_CONCURRENT_UPLOADS;
		}
		UploadTaskManager.getInstance().setConcurrentUploads(concurrentUploads);
		concurrentUploadsField.setText(String.valueOf(concurrentUploads));
	}

	private void onConnectionProblemsChanged(Set<? extends Runnable> problems) {
		ImageView connectionStatusImage = (ImageView) connectionStatusLabel.getGraphic();
		Tooltip connectionStatusTooltip = connectionStatusLabel.getTooltip();

		if (problems.isEmpty()) {
			Platform.runLater(() -> {
				connectionStatusImage.setImage(CONNECTION_OK_IMAGE);
				connectionStatusTooltip.setText(ApplicationMain.getMessage("network.ok"));
			});
		} else {
			Platform.runLater(() -> {
				connectionStatusImage.setImage(CONNECTION_ERROR_IMAGE);
				connectionStatusTooltip.setText(ApplicationMain.getMessage("network.problems"));
			});
		}
	}

	private void onUploadSpeedChanged(long newValue) {
		Platform.runLater(() -> {
			uploadSpeedLabel.setText(ApplicationMain.getMessage("network.transfer.speed", Utils.formatFileSize(newValue)));

			if (newValue > 0) {
				long remainSize = pages.getSize() - pages.getUploadedSize();
				long remainTime = (remainSize / newValue) * 1000;
				uploadRemainTimeLabel.setText(ApplicationMain.getMessage("network.transfer.remaintime", Utils.formatDuration(remainTime)));
			}
		});
	}

	public void load(ResourceContainer pages) {
		ResourceUtils.merge(this.pages, pages);
		TreeItem<Resource> root = ResourceExplorerController.buildTree(this.pages);
		resourcesTable.setRoot(root);

		ThreadPoolManager.getInstance().execute(() -> {
			try {
				ResourceManager.getInstance().syncAll(this.pages);
			} catch (IOException e) {
				LOGGER.log(Level.WARNING, "Failed to sync resources", e);
				final Alert alert = new ExceptionAlert(e);
				alert.showAndWait();
				return;
			}

			for (Resource resource : this.pages.getResources()) {
				queueAll(resource);
			}
		});
	}

	private void queueAll(Resource resource) {
		UploadTaskManager.getInstance().queue(resource);

		if (resource instanceof ResourceContainer) {
			for (Resource res : ((ResourceContainer) resource).getResources()) {
				queueAll(res);
			}
		}
	}
}
