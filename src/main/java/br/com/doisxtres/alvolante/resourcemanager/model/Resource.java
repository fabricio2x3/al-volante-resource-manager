package br.com.doisxtres.alvolante.resourcemanager.model;

import java.io.File;
import java.util.Date;

import javafx.beans.binding.Binding;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Resource implements Comparable<Resource> {
	protected final File file;
	private ResourceContainer parent = null;
	private Integer id = null;
	private final ObjectProperty<ResourceStatus> status = new SimpleObjectProperty<>(new ResourceStatus());

	protected final StringProperty name = new SimpleStringProperty();
	protected final ObjectProperty<Long> size = new SimpleObjectProperty<>(0L);
	protected final ObjectProperty<Long> uploadedSize = new SimpleObjectProperty<>(0L);
	protected final ObjectProperty<Date> uploadStart = new SimpleObjectProperty<>();
	protected final ObjectProperty<Date> uploadEnd = new SimpleObjectProperty<>();
	protected final StringProperty checksum = new SimpleStringProperty();
	protected final StringProperty uploadedChecksum = new SimpleStringProperty();

	// TODO: Move to CellFactory
	private Binding<String> integrityBindind = new StringBinding() {
		{
			bind(checksum, uploadedChecksum);
		}

		@Override
		protected String computeValue() {
			return getIntegrity();
		}
	};

	public Resource(File file) {
		String name = file.getName().trim();
		if ("-".equals(name)) {
			name = "";
		}

		this.file = file;
		this.name.set(name);
		this.size.set(file.length());
	}

	protected Resource(String name) {
		this.file = null;
		this.name.set(name);
	}

	public File getFile() {
		return file;
	}

	public ResourceContainer getParent() {
		return parent;
	}

	public void setParent(ResourceContainer parent) {
		this.parent = parent;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ResourceType getType() {
		if (getParent() == null) {
			return ResourceType.ROOT;
		}
		return ResourceType.values()[getParent().getType().ordinal() + 1];
	}

	public ResourceStatus getStatus() {
		return status.get();
	}

	public String getName() {
		return name.get();
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public long getSize() {
		return size.get();
	}

	public long getUploadedSize() {
		return uploadedSize.get();
	}

	public void setUploadedSize(long uploadedSize) {
		this.uploadedSize.set(uploadedSize);
	}

	public void incrementUploadedSize(long value) {
		synchronized (uploadedSize) {
			long newSize = uploadedSize.get() + value;
			if (newSize > getSize()) {
				newSize = getSize();
			}
			this.uploadedSize.set(newSize);
		}
	}

	public Date getUploadStart() {
		return uploadStart.get();
	}

	public void setUploadStart(Date uploadStart) {
		this.uploadStart.set(uploadStart);
	}

	public Date getUploadEnd() {
		return uploadEnd.get();
	}

	public void setUploadEnd(Date uploadEnd) {
		this.uploadEnd.set(uploadEnd);
	}

	public String getChecksum() {
		return checksum.get();
	}

	public void setChecksum(String checksum) {
		this.checksum.set(checksum);
	}

	public String getUploadedChecksum() {
		return uploadedChecksum.get();
	}

	public void setUploadedChecksum(String uploadedChecksum) {
		this.uploadedChecksum.set(uploadedChecksum);
	}

	public String getIntegrity() {
		// Initial state: upload in progress
		if (getChecksum() == null || getUploadedChecksum() == null) {
			return "";
		}
		return getChecksum().equalsIgnoreCase(getUploadedChecksum()) ? "OK" : "Corrompido";
	}

	public double getProgress() {
		if (getSize() == 0) {
			return 1d;
		}
		return (double) getUploadedSize() / getSize();
	}

	public Property<String> getNameProperty() {
		return name;
	}

	public Property<Long> getSizeProperty() {
		return size;
	}

	public Property<Long> getUploadedSizeProperty() {
		return uploadedSize;
	}

	public Property<Date> getUploadStartProperty() {
		return uploadStart;
	}

	public Property<Date> getUploadEndProperty() {
		return uploadEnd;
	}

	public Binding<String> integrityBindind() {
		return integrityBindind;
	}

	public Property<ResourceStatus> statusProperty() {
		return status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		Resource other = (Resource) obj;
		if (file == null) {
			if (other.file != null) {
				return false;
			}
		} else if (!file.equals(other.file)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		Resource parent = getParent();
		String name = getName();
		return (parent != null ? parent.toString() + "/" : "") + (name != null ? name : " ");
	}

	@Override
	public int compareTo(Resource o) {
		return toString().compareTo(o.toString());
	}
}
