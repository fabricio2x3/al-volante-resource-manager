package br.com.doisxtres.alvolante.resourcemanager.core.upload;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import br.com.doisxtres.alvolante.resourcemanager.model.Resource;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceStatusType;
import br.com.doisxtres.alvolante.resourcemanager.model.ResourceType;

public final class UploadTaskManager {
	public static final int MAX_CONCURRENT_UPLOADS = 10;
	
	private final Map<Resource, ResourceUploadTask> tasks = new HashMap<>();
	private final BlockingQueue<Runnable> queue = new PriorityBlockingQueue<>(MAX_CONCURRENT_UPLOADS);
	private final ThreadPoolExecutor threadPool = new ThreadPoolExecutor(1, MAX_CONCURRENT_UPLOADS, 0L, TimeUnit.MILLISECONDS, queue, new UploadThreadFactory());
	
	protected UploadTaskManager() {
	}
	
	public synchronized boolean queue(Resource resource) {
		// Check the resource type: only upload files
		if (resource.getType() != ResourceType.RESOURCE) {
			return false;
		}
		
		// Check if there is already a task for this resource
		if (tasks.containsKey(resource)) {
			return false;
		}
		
		// Create the task and send to execution
		final ResourceUploadTask task = new ResourceUploadTask(resource);
		tasks.put(resource, task);
		threadPool.execute(task);
		resource.getStatus().setStatus(ResourceStatusType.QUEUED, task);
		return true;
	}
	
	public synchronized void setPriority(Resource resource, int priority) {
		// Try to find the task for this resource
		final ResourceUploadTask task = tasks.get(resource);
		if (task == null) {
			return;
		}
		
		// Try to remove the task from the thread pool
		boolean removed = threadPool.remove(task);
		// If removed, set the priority and re-send to execution
		if (removed) {
			task.setPriority(priority);
			threadPool.execute(task);
		}
	}
	
	public void shutdown() {
		threadPool.shutdownNow();
	}
	
	public void setConcurrentUploads(int size) {
		threadPool.setCorePoolSize(size);
	}
	
	public static UploadTaskManager getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder {
		protected static final UploadTaskManager INSTANCE = new UploadTaskManager();
	}
}
