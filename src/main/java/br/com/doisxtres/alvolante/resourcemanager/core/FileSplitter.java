package br.com.doisxtres.alvolante.resourcemanager.core;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

public final class FileSplitter implements Iterator<File>, Closeable {
	private static final int BUFFER_SIZE = 4096;
	
	private final File source;
	private final long partLength;
	private final InputStream in;
	private final int numberOfParts;
	private int currentPart = 0;

	public FileSplitter(File source, long partLength) throws FileNotFoundException {
		this.source = source;
		this.partLength = partLength;
		this.in = new FileInputStream(source);
		this.numberOfParts = (int) Math.ceil(1d * source.length() / partLength);
	}
	
	public int getNumberOfParts() {
		return numberOfParts;
	}

	@Override
	public boolean hasNext() {
		return currentPart < numberOfParts;
	}

	@Override
	public File next() {
		currentPart++;
		try {
			// Create a new file for this part
			final File part = File.createTempFile(source.getName(), ".part");

			// Read from the source file and write to the part file
			long partRemain = partLength;
			try (OutputStream out = new FileOutputStream(part)) {
				final byte[] buffer = new byte[BUFFER_SIZE];
				
				int read = 0;
				while ((read = in.read(buffer, 0, (int) Math.min(buffer.length, partRemain))) > 0) {
					partRemain -= read;
					out.write(buffer, 0, read);
				}
			}
			
			// Nothing was read
			if (partRemain == partLength) {
				return null;
			}

			// Return the part file
			return part;
		} catch (IOException e) {
			throw new RuntimeException("File splitting failed", e);
		}
	}

	@Override
	public void close() throws IOException {
		in.close();
	}
}
