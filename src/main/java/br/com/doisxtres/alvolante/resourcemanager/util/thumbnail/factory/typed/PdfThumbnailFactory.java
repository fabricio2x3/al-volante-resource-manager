package br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.typed;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import javax.imageio.ImageIO;

import br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory.RegexThumbnailFactory;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

public class PdfThumbnailFactory extends RegexThumbnailFactory {
	public PdfThumbnailFactory() {
		super("(.*(\\.(?i)(pdf))$)");
	}

	@Override
	protected File createImpl(File source) throws IOException {
		RandomAccessFile raf = new RandomAccessFile(source, "r");
		FileChannel channel = raf.getChannel();
		ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
		PDFFile pdf = new PDFFile(buf);
		PDFPage page = pdf.getPage(0);
		File output = File.createTempFile("thumbnail", ".jpg");

		// create the image
		Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(), (int) page.getBBox().getHeight());
		BufferedImage bufferedImage = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);

		Image image = page.getImage(rect.width, rect.height, // width & height
				rect, // clip rect
				null, // null for the ImageObserver
				true, // fill background with white
				true // block until drawing is done
				);
		Graphics2D bufImageGraphics = bufferedImage.createGraphics();
		bufImageGraphics.drawImage(image, 0, 0, null);

		ImageIO.write(bufferedImage, "jpeg", output);
		raf.close();

		return output;
	}
}
