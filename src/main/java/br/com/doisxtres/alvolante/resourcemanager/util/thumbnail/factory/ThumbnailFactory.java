package br.com.doisxtres.alvolante.resourcemanager.util.thumbnail.factory;

import java.io.File;
import java.io.IOException;

public interface ThumbnailFactory {
	boolean accept(File source);
	File create(File source) throws IOException;
}
