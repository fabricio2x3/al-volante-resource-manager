package br.com.doisxtres.alvolante.resourcemanager.model;

import java.io.File;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

public class ResourceContainer extends Resource {
	private final Set<Resource> resources = new TreeSet<>();

	public ResourceContainer(File file) {
		super(file);
	}

	public ResourceContainer(String name) {
		super(name);
	}

	public Set<Resource> getResources() {
		return Collections.unmodifiableSet(resources);
	}

	public boolean addResource(Resource resource) {
		resource.setParent(this);
		boolean ret = resources.add(resource);

		if (ret) {
			resource.size.addListener((observable) -> {
				size.get();
				size.set(getSize());
			});
			size.set(getSize());
			resource.uploadedSize.addListener((observable) -> {
				uploadedSize.get();
				uploadedSize.set(getUploadedSize());
			});
			resource.uploadStart.addListener((observable) -> {
				uploadStart.get();
				uploadStart.set(getUploadStart());
			});
			resource.uploadEnd.addListener((observable) -> {
				uploadEnd.get();
				uploadEnd.set(getUploadEnd());
			});
		}

		return ret;
	}

	public Resource findResource(Object indetifier) {
		for (Resource res : resources) {
			if (res.equals(indetifier)) {
				return res;
			}
		}
		return null;
	}

	@Override
	public long getSize() {
		long size = 0;
		for (Resource resource : resources) {
			size += resource.getSize();
		}
		return size;
	}

	@Override
	public long getUploadedSize() {
		long uploadedSize = 0;
		for (Resource resource : resources) {
			uploadedSize += resource.getUploadedSize();
		}
		return uploadedSize;
	}

	@Override
	public void setUploadedSize(long uploadedSize) {
		// Do nothing
	}

	@Override
	public Date getUploadStart() {
		Date uploadStart = null;
		// Find the date of the first started upload
		for (Resource resource : resources) {
			if (resource.getUploadStart() == null) {
				continue;
			}
			if (uploadStart == null || uploadStart.after(resource.getUploadStart())) {
				uploadStart = resource.getUploadStart();
			}
		}
		return uploadStart;
	}

	@Override
	public void setUploadStart(Date uploadStart) {
		// Do nothing
	}

	@Override
	public Date getUploadEnd() {
		Date uploadEnd = null;
		// Find the date of the last finished upload
		for (Resource resource : resources) {
			// Check if there is any unfinished upload. If there is, this is not finished yet
			if (resource.getUploadEnd() == null) {
				return null;
			}
			if (uploadEnd == null || uploadEnd.before(resource.getUploadEnd())) {
				uploadEnd = resource.getUploadEnd();
			}
		}
		return uploadEnd;
	}

	@Override
	public void setUploadEnd(Date uploadEnd) {
		// Do nothing
	}
}
