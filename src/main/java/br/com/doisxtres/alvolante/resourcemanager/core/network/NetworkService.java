package br.com.doisxtres.alvolante.resourcemanager.core.network;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.beans.property.SetProperty;
import javafx.beans.property.SimpleSetProperty;
import javafx.collections.FXCollections;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import br.com.doisxtres.alvolante.resourcemanager.Config;
import br.com.doisxtres.alvolante.resourcemanager.core.upload.MultipartUploadIdentifier;
import br.com.doisxtres.alvolante.resourcemanager.core.upload.UploadResponse;
import br.com.doisxtres.alvolante.resourcemanager.model.RemoteResource;
import br.com.doisxtres.alvolante.resourcemanager.model.Resource;
import br.com.doisxtres.alvolante.resourcemanager.model.User;

public class NetworkService {
	private static final Logger LOGGER = Logger.getLogger(NetworkService.class.getName());
	
	private final SetProperty<Runnable> networkProblems = new SimpleSetProperty<>(FXCollections.observableSet());
	private final NetworkSpeedometer uploadSpeedometer = new NetworkSpeedometer();
	
	private final CloseableHttpClient client;
	private final CredentialsProvider credentialsProvider;
	private final HttpHost host;

	protected NetworkService() {
		credentialsProvider = new BasicCredentialsProvider();
		client = HttpClientBuilder.create().setDefaultCredentialsProvider(credentialsProvider).build();
		host = new HttpHost(Config.getRemoteHost(), Config.getRemotePort(), Config.getRemoteSchema());
	}
	
	public void login(String username, String password) {
		credentialsProvider.setCredentials(new AuthScope(host), new UsernamePasswordCredentials(username, password));
	}

	public HttpResponse logout() throws IOException {
		HttpGet request = new HttpGet("/logout");
		return client.execute(host, request);
	}
	
	public User getUser() throws IOException {
		HttpGet request = new HttpGet("/api/user.json");
		return client.execute(host, request, new JsonResponseHandler<>(User.class));
	}
	
	public UploadResponse upload(Resource resource) throws IOException {
		HttpPost request = new HttpPost("/api/upload");
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addPart("file", new ResourceFileBody(resource));
		request.setEntity(builder.build());
		
		return client.execute(host, request, new JsonResponseHandler<>(UploadResponse.class));
	}
	
	public UploadResponse upload(File file) throws IOException {
		HttpPost request = new HttpPost("/api/upload");
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addPart("file", new FileBody(file));
		request.setEntity(builder.build());
		
		return client.execute(host, request, new JsonResponseHandler<>(UploadResponse.class));
	}
	
	public MultipartUploadIdentifier initiateMultipartUpload(String filename) throws IOException {
		final HttpPost request = new HttpPost("/api/initiate-multipart-upload");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("filename", filename));
		request.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
		return client.execute(host, request, new JsonResponseHandler<>(MultipartUploadIdentifier.class));
	}
	
	public String uploadPart(String tmpPath, String uploadId, int partNumber, ContentBody part) throws IOException {
		final HttpPost request = new HttpPost("/api/upload-part");
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addPart("tmpPath", new StringBody(tmpPath, ContentType.TEXT_PLAIN));
		builder.addPart("uploadId", new StringBody(uploadId, ContentType.TEXT_PLAIN));
		builder.addPart("partNumber", new StringBody(String.valueOf(partNumber), ContentType.TEXT_PLAIN));
		builder.addPart("file", part);
		builder.setCharset(StandardCharsets.UTF_8);
		request.setEntity(builder.build());
		return client.execute(host, request, new BasicResponseHandler());
	}
	
	public String completeMultipartUpload(String tmpPath, String uploadId, Iterable<String> partIds) throws IOException {
		final HttpPost request = new HttpPost("/api/complete-multipart-upload");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("tmpPath", tmpPath));
		nvps.add(new BasicNameValuePair("uploadId", uploadId));
		for (String partId : partIds) {
			nvps.add(new BasicNameValuePair("partIds[]", partId));
		}
		request.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
		return client.execute(host, request, new BasicResponseHandler());
	}
	
	public RemoteResource findPage(String name) throws IOException {
		final HttpPost request = new HttpPost("/api/find-page");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("name", name));
		request.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
		return client.execute(host, request, new JsonResponseHandler<>(RemoteResource.class));
	}
	
	public RemoteResource findSubpage(int pageId, String name) throws IOException {
		final HttpPost request = new HttpPost("/api/find-subpage");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("pageId", String.valueOf(pageId)));
		nvps.add(new BasicNameValuePair("name", name));
		request.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
		return client.execute(host, request, new JsonResponseHandler<>(RemoteResource.class));
	}
	
	public RemoteResource findSection(int subpageId, String name) throws IOException {
		final HttpPost request = new HttpPost("/api/find-section");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("subpageId", String.valueOf(subpageId)));
		nvps.add(new BasicNameValuePair("name", name));
		request.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
		return client.execute(host, request, new JsonResponseHandler<>(RemoteResource.class));
	}
	
	public RemoteResource createResource(int sectionId, String name) throws IOException {
		final HttpPost request = new HttpPost("/api/create-resource");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("sectionId", String.valueOf(sectionId)));
		nvps.add(new BasicNameValuePair("name", name));
		request.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
		return client.execute(host, request, new JsonResponseHandler<>(RemoteResource.class));
	}
	
	public RemoteResource updateResource(int id, String remoteFileName, String remoteThumbnailName) throws IOException {
		final HttpPost request = new HttpPost("/api/update-resource");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("id", String.valueOf(id)));
		nvps.add(new BasicNameValuePair("fileName", remoteFileName));
		nvps.add(new BasicNameValuePair("thumbnail", remoteThumbnailName));
		request.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
		return client.execute(host, request, new JsonResponseHandler<>(RemoteResource.class));
	}
	
	public void shutdown() {
		try {
			client.close();
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, e.getMessage(), e);
		}
	}
	
	public SetProperty<Runnable> networkProblemsProperty() {
		return networkProblems;
	}
	
	public NetworkSpeedometer getUploadSpeedometer() {
		return uploadSpeedometer;
	}

	public static NetworkService getInstance() {
		return SingletonHolder.INSTANCE;
	}

	private static class SingletonHolder {
		protected static final NetworkService INSTANCE = new NetworkService();
	}
}
